package com.aloha4you;

import java.io.Console;
import java.sql.*;
import java.sql.Date;
import java.util.*;

public class Requests {
    private static ResultSet reqResultSet = null;
    private static int coloumnCnt = 0;
    public static int rowCnt = 0;


    public static ResultSet getReqResultSet() {
        return reqResultSet;
    }
    static List<String> tables;
    public static String name;
    static {
        tables = new ArrayList<>();
        try {
            ResultSet rs = Client.getSTATEMENT().executeQuery("Show tables");
            while(rs.next())
                tables.add(rs.getString(1));
        } catch (SQLException throwables) {
            System.out.println("Ошибка при загрузке имен таблиц");
        }
    }
    static Scanner a = new Scanner(System.in);
    //запрос на удаление

    public static void showTables(){
        Client.createDefaultClient();
        a = new Scanner(System.in);
        for(String s : tables)
            System.out.println(s);
    }
    public static void statRequest(int s) throws SQLException {
        Client.createDefaultClient();
        if(s == 1){
            System.out.println("Узнать врачей с определенным опытом по границе справа - 1");
            System.out.println("Узнать врачей с определенным опытом по границе слева - 2");
            System.out.println("Узнать врачей с определенным опытом в промежутке(включительно) - 3");
            int t = Integer.valueOf(a.nextLine().trim());
            int t1 = 0;
            int t2 = 0;
            if(t == 1 || t == 2){
                System.out.println("Введите опыт(целое число, в годах)");
                t1 = Integer.valueOf(a.nextLine().trim());
                if(t == 1)
                    reqResultSet = Client.getSTATEMENT().executeQuery("select (SELECT count(id_of_doctor) from doctor where experience < "+t1+")/count(id_of_doctor)*100 as Искомый_процент from doctor");
                else
                    reqResultSet = Client.getSTATEMENT().executeQuery("select (SELECT count(id_of_doctor) from doctor where experience > "+t1+")/count(id_of_doctor)*100 as Искомый_процент from doctor");

            }
            else if(t == 3){
                System.out.println("Введите опыт(два числа, нажмите энтер как разделитель)");
                t1 = Integer.valueOf(a.nextLine().trim());
                t2 = Integer.valueOf(a.nextLine().trim());
                reqResultSet = Client.getSTATEMENT().executeQuery("select (SELECT count(id_of_doctor) from doctor where experience >= "+t1+" and experience <= "+t2+" )/count(id_of_doctor)*100 as Искомый_процент from doctor");
            }
            coloumnCnt = reqResultSet.getMetaData().getColumnCount();
            while (reqResultSet.next()){
                System.out.println();
                for(int i = 1;i<=coloumnCnt;i++)
                    System.out.print(reqResultSet.getString(i)+"\t");
            }
        }
        else if (s == 2){
            reqResultSet = Client.getSTATEMENT().executeQuery("Select place from complaint\n" +
                    " group by place\n" +
                    " Having count(place) = (Select count(place) from complaint group by place ORDER by count(place) desc LIMIT 1);");
        coloumnCnt = reqResultSet.getMetaData().getColumnCount();
        while (reqResultSet.next()){
            System.out.println();
            for(int i = 1;i<=coloumnCnt;i++)
                System.out.printf("%-30s",reqResultSet.getString(i));
        }
        }
        else if (s == 3){
            reqResultSet = Client.getSTATEMENT().executeQuery(
                    "select surname,firstname,patronymic, count(surname)\n" +
                    "FROM doc_sch_cab_vis\n" +
                    "natural join people\n" +
                    "natural join doctor\n" +
                    "group by id_of_people" +
                            " order by count(surname) desc;");
            coloumnCnt = reqResultSet.getMetaData().getColumnCount();
            while (reqResultSet.next()){
                System.out.println();
                for(int i = 1;i<=coloumnCnt;i++)
                    System.out.printf("%-30s",reqResultSet.getString(i));
            }
        }
    }
    public static void deleteRequest(boolean isTable,String name) throws SQLException {
        Client.createDefaultClient();
        a = new Scanner(System.in);
        if (isTable) {
            Client.getSTATEMENT().executeUpdate("DROP TABLE " + name);
            System.out.println("Ваш запрос успешно выполнен!");
        }
            else {
                System.out.println("Вы уверены, что хотите удалить базу данных '" + name + "'? 1/0");
                if (a.nextInt() == 1) {
                    Client.getSTATEMENT().executeUpdate("DROP DATABASE " + name);
                    System.out.println("База данных "+name+"успешно удалена!");
                }
            }
    }

    //запрос на редактирование
    public static void updateRequest() throws SQLException {
        Client.createDefaultClient();
        a = new Scanner(System.in);
        System.out.println("Таблицы:");
        showTables();
        System.out.println("Введите название таблицы, которую необходимо изменить:\n");
        String t = a.nextLine();
        reqResultSet = (Client.getSTATEMENT().executeQuery("SELECT * FROM "+t));
        coloumnCnt = reqResultSet.getMetaData().getColumnCount();
        System.out.println("Вы хотите добавить строчку? - 1");
        String s = a.nextLine();
        List<String> l = new ArrayList<>();
        List<String> TYPES = new ArrayList<>();
        if(s.equals("1")){
            while (true){
                for(int i = 2; i <= coloumnCnt;i++){
                    System.out.println("Введите данные в слеудющий столбец:");
                    System.out.println(reqResultSet.getMetaData().getColumnName(i)+" - "+reqResultSet.getMetaData().getColumnTypeName(i));
                    l.add(a.nextLine());
                    TYPES.add(reqResultSet.getMetaData().getColumnTypeName(i));
                }
                System.out.println("Если введенные данные верны и их необходимо добавить в таблицу,а затем выйти в главное меню, введите 1");
                System.out.println("Если вы хотите покинуть редактирование и перейти в главное меню, введите 2");
                System.out.println("Если введенные данные неверны но вы хотите ввести все заново, введите 3");
                String tmp = a.nextLine();
                if(tmp.equals("1")){
                    StringBuilder sql = new StringBuilder("INSERT INTO " +t+" Values (NULL,");
                    for(int j = 2; j <= coloumnCnt; j++){
                        if(j == coloumnCnt)
                            sql.append("?)");
                        else
                            sql.append("?,");
                    }
                    PreparedStatement preparedStatement = Client.getCONNECTION().prepareStatement(sql.toString());
                    for(int d = 1;d <= coloumnCnt-1; d++){
                        if(TYPES.get(d-1).equals("INT"))
                            preparedStatement.setInt(d, Integer.valueOf(l.get(d-1)));
                        else if(TYPES.get(d-1).equals("CHAR") || TYPES.get(d-1).equals("VARCHAR"))
                            preparedStatement.setString(d, l.get(d-1));
                        else if(TYPES.get(d-1).equals("DATE"))
                            preparedStatement.setDate(d, Date.valueOf(l.get(d-1)));
                    }
                    int rows = preparedStatement.executeUpdate();
                    System.out.printf("%d rows added", rows);
                    break;
                }
                else if(tmp.equals("2")){
                    break;
                }
                else if(tmp.equals("3")){
                    l.clear();
                    continue;
                }
                else{
                    System.out.println("Введен неправильный код, выход в главное меню");
                    break;
                }
            }
        }
    }

    public static void profiRequest() throws SQLException {
        Client.createDefaultClient();
        a = new Scanner(System.in);
        System.out.println("Введите ваш корректный запрос в одну строчку(!):\n");
        reqResultSet = Client.getSTATEMENT().executeQuery(a.nextLine());
        System.out.println("Вывести результат из указанного SELECT? true/false");
        if(a.nextBoolean() == true)
        coloumnCnt = reqResultSet.getMetaData().getColumnCount();
        while (reqResultSet.next()){
            System.out.println();
            for(int i = 1;i<=coloumnCnt;i++)
                System.out.print(reqResultSet.getString(i)+"\t");
        }
    }

    //запрос на создание таблицы
    public static void alterRequest() throws SQLException {
        Client.createDefaultClient();
        a = new Scanner(System.in);
        System.out.println("Выберите, что необходимо сделать:");
        System.out.println("1 - удалить строку по её id");
        System.out.println("2 - удалить строку/строки по некоторому условию");
        System.out.println("3 - заменить значение столбца в строке");
        int tmp = Integer.valueOf(a.nextLine().trim());
        if(tmp == 1){
            showAllData();
            System.out.println("\nВведите необходимый id для удаления");
            int tmpdelete = Integer.valueOf(a.nextLine().trim());
            System.out.println("Вы точно хотите удалить строку с id == "+tmpdelete+" этой таблицы? y/n");
            String isYes = a.nextLine().toLowerCase().trim();
            if(isYes.equals("y")){
                int rows = (Client.getSTATEMENT().executeUpdate("DELETE FROM "+name+" WHERE id_of_"+name+" = "+ tmpdelete));
                System.out.printf("%d rows removed", rows);
            }
        }
        else if(tmp == 2){
            System.out.println("Напишите название изменяемой таблицы:");
            name = a.nextLine();
            StringBuilder sqlWhereCondition = new StringBuilder();
            Map<String,String> map = new HashMap<>();
            List<String> choosedCol = new ArrayList<>();
            reqResultSet = (Client.getSTATEMENT().executeQuery("SELECT * FROM "+name));
            coloumnCnt = reqResultSet.getMetaData().getColumnCount();
            System.out.println("Данные о столбцах таблицы:");
            for(int i = 1;i<=coloumnCnt;i++){
                System.out.print(reqResultSet.getMetaData().getColumnName(i)+" - "+reqResultSet.getMetaData().getColumnTypeName(i)+"\n");
                map.put(reqResultSet.getMetaData().getColumnName(i),reqResultSet.getMetaData().getColumnTypeName(i));
            }
            System.out.println("По одному или нескольким столбцам будет условие удаления? Введите 1/2");
            int ttmmpp = Integer.valueOf(a.nextLine().trim());
            if(ttmmpp == 1){
                System.out.println("Введите название столбца");
                String tmp3 = a.nextLine().trim().toLowerCase();
                choosedCol.add(tmp3);
                //Наверное, отдельная функция
                sqlWhereCondition.append(returnWherePredicate(map,choosedCol,0));
                System.out.println("Ваше условие для удаления:\t"+sqlWhereCondition.toString());
                System.out.println("Подтверждаете удаление? y/n");
                String tmp4 = a.nextLine().trim().toLowerCase();
                if(tmp4.equals("y")){
                    int rows = (Client.getSTATEMENT().executeUpdate("DELETE FROM "+name+" WHERE "+ sqlWhereCondition.toString()));
                    System.out.printf("%d rows removed", rows);
                }
            }
            else if(ttmmpp == 2){
                System.out.println("Введите количество вводимых условий:");
                int tmp2 = Integer.valueOf(a.nextLine().trim());
                for(int i = 0; i< tmp2;i++){
                    System.out.println("Введите название столбца для "+i+"-го условия удаления");
                    String tmp3 = a.nextLine().trim().toLowerCase();
                    choosedCol.add(tmp3);
                }
                for(int i = 0; i< tmp2;i++){
                    if(i!=0)
                        sqlWhereCondition.append(" and ");
                    sqlWhereCondition.append(returnWherePredicate(map,choosedCol,i));
                }
                System.out.println("Ваше условие для удаления:\t"+sqlWhereCondition.toString());
                System.out.println("Подтверждаете удаление? y/n");
                String tmp4 = a.nextLine().trim().toLowerCase();
                if(tmp4.equals("y")){
                    int rows = (Client.getSTATEMENT().executeUpdate("DELETE FROM "+name+" WHERE "+ sqlWhereCondition.toString()));
                    System.out.printf("%d rows removed", rows);
                }
            }
        }
        else if(tmp == 3){
            StringBuilder sqlWhereCondition = new StringBuilder();
            String sqlSetCondition = "";
            Map<String,String> map = new HashMap<>();
            String choosedCol = "";
            System.out.println("Напишите название изменяемой таблицы:");
            name = a.nextLine();
            reqResultSet = (Client.getSTATEMENT().executeQuery("SELECT * FROM "+name));
            coloumnCnt = reqResultSet.getMetaData().getColumnCount();
            System.out.println("Данные о столбцах таблицы:");
            for(int i = 1;i<=coloumnCnt;i++){
                System.out.println(reqResultSet.getMetaData().getColumnName(i)+" - "+reqResultSet.getMetaData().getColumnTypeName(i)+"\n");
                map.put(reqResultSet.getMetaData().getColumnName(i),reqResultSet.getMetaData().getColumnTypeName(i));
            }
            System.out.println("Введите название столбца, которое будем редактировать");
            choosedCol = a.nextLine().trim().toLowerCase();
            int rows = 0;
            System.out.println("Возможные преобразования:");
            if(map.get(choosedCol).equals("VARCHAR") || map.get(choosedCol).equals("CHAR")) {
                System.out.println("Строка: замена на новое значение у всех строк - 1");
                System.out.println("Строка: замена на новое значение у строк по условию - 2");
                System.out.println("Введите 1/2");
                int t = Integer.valueOf(a.nextLine());
                System.out.println("Введите новое значение для строк столбца "+choosedCol+"(Важно! Надо написать в кавчках!):");
                sqlSetCondition = a.nextLine();
                if(t == 1){
                    rows = (Client.getSTATEMENT().executeUpdate("UPDATE "+name+" SET " + choosedCol+" = "+sqlSetCondition));
                }
                if(t == 2){
                    System.out.println("Напишите название столбца, по которому будет проверяться условие:");
                    String tmpCol = a.nextLine().toLowerCase().trim();
                    sqlWhereCondition.append(returnWherePredicate(map,List.of(tmpCol),0));
                    rows = (Client.getSTATEMENT().executeUpdate("UPDATE "+name+" SET " + choosedCol+" = "+sqlSetCondition+" WHERE "+ sqlWhereCondition.toString()));
                }
            }
            else if(map.get(choosedCol).equals("INT")) {
                System.out.println("Число: замена на новое значение у всех строк - 3");
                System.out.println("Число: замена на новое значение у строк по условию - 4");
                System.out.println("Число: замена на новое значение у всех строк посредством арифмитической операции этого числа с константой - 5\n(Пример: хотите снизить цену в два раза, т.е. Цена/2)");
                System.out.println("Число: замена на новое значение у строк посредством арифмитической операции с константой по некоторому условию - 6\n(Пример: хотите увеличить цену на 20 только у тех, кто соответствует условию)");
                System.out.println("Введите 3/4/5/6");
                int t = Integer.valueOf(a.nextLine());
                if(t == 3){
                    System.out.println("Введите новое значение для строк столбца "+choosedCol+"(Важно! Надо написать обычное число без ковычек!):");
                    sqlSetCondition = a.nextLine().trim();
                    rows = (Client.getSTATEMENT().executeUpdate("UPDATE "+name+" SET " + choosedCol+" = "+sqlSetCondition));
                }
                else if(t == 4){
                    System.out.println("Введите новое значение для строк столбца "+choosedCol+"(Важно! Надо написать обычное число без ковычек!):");
                    sqlSetCondition = a.nextLine().trim();
                    System.out.println("Напишите название столбца, по которому будет проверяться условие:");
                    String tmpCol = a.nextLine().toLowerCase().trim();
                    sqlWhereCondition.append(returnWherePredicate(map,List.of(tmpCol),0));
                    rows = (Client.getSTATEMENT().executeUpdate("UPDATE "+name+" SET " + choosedCol+" = "+sqlSetCondition+" WHERE "+ sqlWhereCondition.toString()));
                }
                else if(t == 5){
                    System.out.println("Напишите, что надо сделать с значением столбца БЕЗ КОВЫЧЕК!:\nПример: название_столбца*5,100-название_столбца");
                    sqlSetCondition = a.nextLine().trim();
                    rows = (Client.getSTATEMENT().executeUpdate("UPDATE "+name+" SET " + choosedCol+" = "+sqlSetCondition));
                }
                else if(t == 6){
                    System.out.println("Напишите, что надо сделать с значением столбца БЕЗ КОВЫЧЕК!:\nПример: название_столбца*5,100 - название_столбца");
                    sqlSetCondition = a.nextLine().trim();
                    System.out.println("Напишите название столбца, по которому будет проверяться условие:");
                    String tmpCol = a.nextLine().toLowerCase().trim();
                    sqlWhereCondition.append(returnWherePredicate(map,List.of(tmpCol),0));
                    String query = "UPDATE "+name+" SET " + choosedCol+" = "+sqlSetCondition+" WHERE "+ sqlWhereCondition.toString();
                    rows = (Client.getSTATEMENT().executeUpdate("UPDATE "+name+" SET " + choosedCol+" = "+sqlSetCondition+" WHERE "+ sqlWhereCondition.toString()));
                }
            }
            System.out.printf("%d rows updated", rows);
        }
    }

    public static String returnWherePredicate(Map m,List l,int index){
        System.out.println("------------------------------------------");
        System.out.println("Определение "+index+1+"-го условия по введенному столбцу:");
        if(m.get(l.get(index)).equals("INT")){
            System.out.println("Определить условие колонки по границе справа(включительно) - 1");
            System.out.println("Определить условие колонки по границе слева(включительно) - 2");
            System.out.println("Определить условие колонки в промежутке(включительно) - 3");
            System.out.println("Определить условие колонки по равенству  - 4");
            int t = Integer.valueOf(a.nextLine().trim());
            int t1 = 0;
            int t2 = 0;
            if(t == 1 || t == 2 || t == 4){
                System.out.println("Введите целое число");
                t1 = Integer.valueOf(a.nextLine().trim());
                if(t == 1)
                    return l.get(index)+" <= "+t1+" ";
                else if(t == 2)
                    return l.get(index)+" >= "+t1+" ";
                else
                    return l.get(index)+" = "+t1+" ";
            }
            else if(t == 3){
                System.out.println("Введите опыт(два числа, нажмите энтер как разделитель)");
                t1 = Integer.valueOf(a.nextLine().trim());
                t2 = Integer.valueOf(a.nextLine().trim());
                return l.get(index)+" >= "+t1+" and "+l.get(index)+" <= "+t2+" ";
            }
        }
        else if (m.get(l.get(index)).equals("CHAR")|| m.get(l.get(index)).equals("VARCHAR")){
            System.out.println("Определить условие колонки как неравенство какому-то значению - 1");
            System.out.println("Определить условие колонки как равенство какому-то значению - 2");
            int t = Integer.valueOf(a.nextLine().trim());
            System.out.println("Введите ВНИМАТЕЛЬНО это значение и нажмите enter:");
            String tmp = a.nextLine().trim();
            if(t == 1)
                return l.get(index)+" != \""+tmp+"\" ";
            else if(t == 2){
                return l.get(index)+" = \""+tmp+"\" ";
            }
        }
        return " ";
    }

    public static void showAllData() throws SQLException {
        Client.createDefaultClient();
        a = new Scanner(System.in);
        System.out.println("Чтобы вывести все данные таблицы, введите ее название, иначе введите no. name_table/no");
        if((name = a.nextLine()) != "no"){
            reqResultSet = (Client.getSTATEMENT().executeQuery("SELECT * FROM "+name));
            coloumnCnt = reqResultSet.getMetaData().getColumnCount();
            System.out.println("Данные о столбцах таблицы:");
            for(int i = 1;i<=coloumnCnt;i++){
                System.out.printf("%-30s",reqResultSet.getMetaData().getColumnName(i)+" - "+reqResultSet.getMetaData().getColumnTypeName(i));
            }
            while (reqResultSet.next()){
                System.out.println();
                for(int i = 1;i<=coloumnCnt;i++) {
                    System.out.printf("%-30s",reqResultSet.getString(i));
                }
            }
        }
    }
}
