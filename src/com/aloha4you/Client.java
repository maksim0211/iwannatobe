package com.aloha4you;

import java.io.IOException;
import java.sql.*;

public class Client implements DatabaseClientUtils {
    private static Connection CONNECTION = null;
    private static Statement STATEMENT = null;
    private static ResultSet RESULTSET = null;
    //Ключ, если -1 -> соединение не устанавливалось,если 0 -> ошибка,если 1 -> соединение есть,
    public static int isOk = -1;
    private Client(){
        try {
            CONNECTION = DatabaseClientUtils.setConnectionToDB();
            STATEMENT = DatabaseClientUtils.setStatement();
            isOk = 1;
        } catch (SQLException throwables) {
            isOk = 0;
            System.out.println("Проблема с соединением");
            throwables.printStackTrace();
        }
    }
    private Client(String a,String b,String c){
        try {
            CONNECTION = DatabaseClientUtils.setConnectionWithArgsToDB(a,b,c);
            STATEMENT = DatabaseClientUtils.setStatement();
            isOk = 1;
        } catch (SQLException throwables) {
            System.out.println("Проблема с соединением");
            isOk = 0;
            throwables.printStackTrace();
        }
    }
    public static void closeConnection() throws SQLException {
        getCONNECTION().close();
        setSTATEMENT(null);
    }

    static Client createDefaultClient(){
        return new Client();
    }

    public static Connection getDefaultConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://localhost/dbclinic?serverTimezone=Europe/Moscow&useSSL=false", "root", "Mqa413pty666");
    }

    static Client createClientWithArgs(String a,String b,String c){
        return new Client(a,b,c);
    }

    public static Connection getCONNECTION() {
        return CONNECTION;
    }

    public static void setCONNECTION(Connection CONNECTION) {
        Client.CONNECTION = CONNECTION;
    }

    public static Statement getSTATEMENT() {
        return STATEMENT;
    }

    public static void setSTATEMENT(Statement STATEMENT) {
        Client.STATEMENT = STATEMENT;
    }

    public static ResultSet getRESULTSET() {
        return RESULTSET;
    }

    public static void setRESULTSET(ResultSet RESULTSET) {
        Client.RESULTSET = RESULTSET;
    }

}
