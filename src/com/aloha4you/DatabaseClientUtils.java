package com.aloha4you;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public interface DatabaseClientUtils {

    static Connection setConnectionToDB() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://localhost/dbclinic?serverTimezone=Europe/Moscow&useSSL=false", "root", "Mqa413pty666");
    }

    static Connection setConnectionWithArgsToDB(String url,String log,String pass) throws SQLException {
        return DriverManager.getConnection(url, log, pass);
    }

    static Statement setStatement() throws SQLException {
        return setConnectionToDB().createStatement();
    }
}
