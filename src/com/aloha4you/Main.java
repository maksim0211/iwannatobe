package com.aloha4you;
import java.sql.*;
import java.util.Scanner;

import static com.aloha4you.Requests.*;

public class Main {
    static Scanner b = new Scanner(System.in);
    public static void reloadSc(){
        b = new Scanner(System.in);
    }

    public static void main(String[] args) throws SQLException {
        startMenu();
    }
    public static void startMenu() throws SQLException {
        System.out.println("Starting...");
        System.out.print("Начать работу с базой данных dbclinic или подключиться самостоятельно? Введите dbc/self\n");
        if(b.nextLine().compareTo("dbc") == 0) {
            Client.createDefaultClient();
        }
        else{
            System.out.println("Введите URL базы данных");
            StringBuilder url = new StringBuilder(b.nextLine());
            url.append("?serverTimezone=Europe/Moscow&useSSL=false");
            System.out.println("Введите login");
            String log = b.nextLine();
            System.out.println("Введите password");
            String pass = b.nextLine();
            Client.createClientWithArgs(url.toString(),log,pass);

        }
        if(Client.isOk == 1) {
            System.out.println("Соединение успешно!");
            menuOfDatabase();
            Client.closeConnection();
        }
        else
            System.out.println("Соединение не установлено!");
        Client.closeConnection();
    }
    public static void menuOfDatabase() throws SQLException {
        reloadSc();
        System.out.println("\n----------------DATABASE------MENU------BODY---------------------");
        System.out.println("Выберите, что вы хотите сделать с базой данных:");
        System.out.println("0 - удалить таблицу/базу данных\n1 - добавить строку в таблицу");
        System.out.println("2 - удалить строки из таблицы/редактировать строки таблицы\n3 - показать все таблицы базы данных\n4 - показать все столбцы таблицы\n");
        System.out.println("6 - Аналитические запросы\n9 - Выход");
        int status = b.nextInt();
        if(status == 0) {
            System.out.println("Если удалить таблицу, введите true, иначе введите false");
            if (b.nextBoolean() == true) {
                System.out.println("Введите называние таблицы:");
                reloadSc();
                String a = b.nextLine();
                Requests.deleteRequest(true, a);
            } else {
                reloadSc();
                System.out.println("Введите название базы данных:");
                String db = b.nextLine();
                Requests.deleteRequest(false, db);
            }
            menuOfDatabase();
        }
        else if(status == 1){
            Requests.updateRequest();
        }
        else if(status == 2) {
            alterRequest();
        }
        else if(status == 3){
            showTables();
            System.out.println("\nПоказать значение некоторой таблицы? true/false");
            if(b.nextBoolean() == true)
                Requests.showAllData();
        }
        else if(status == 4){
            showTables();
            Requests.showAllData();
        }
        else if(status == 5){
            Requests.profiRequest();
        }
        else if(status == 6){
            System.out.println("Выберите запрос:");
            System.out.println("Показать процент врачей с определенным стажем - 1");
            System.out.println("Показать самую распространенную жалобу - 2");
            System.out.println("Показать число пациентов отделения - 3");
            int tmp = Integer.valueOf(a.nextLine().trim());
            statRequest(tmp);
        }
        else if(status == 9){
            System.out.println("Exiting...");
            System.exit(200);
        }
        reloadSc();
        Client.closeConnection();
        menuOfDatabase();

    }
}
